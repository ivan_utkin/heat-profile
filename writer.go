package main

import (
	"fmt"
	"github.com/gonum/matrix/mat64"
	"github.com/krjackvinator/heat-profile/lava"
	"io"
	"os"
	"strconv"
)

const (
	dirName = "output"
)

type Point struct {
	X, Y float64
	I, J int
}

type Writer struct {
	flow     *lava.Flow
	Trr      *Transformer
	Trz      *Transformer
	Matrices chan *mat64.Dense
	Times    chan float64
	Nr       int
	Nz       int
}

func (p *Writer) Run() chan bool {
	os.Mkdir(dirName, 0777)
	done := make(chan bool)

	resm := NewDense(p.Nr+1, p.Nz+1)

	go func() {
		for m := range p.Matrices {
			time := <-p.Times
			rows, cols := m.Dims()

			filename := strconv.FormatFloat(time, 'f', 3, 64)
			out, _ := os.Create(dirName + "/" + filename + ".txt")

			coefsr := make([]float64, rows)
			coefsz := make([]float64, cols)

			interpm := NewDense(rows, p.Nz+1)

			eqpr := EqSpPoints(p.Nr)
			eqpz := EqSpPoints(p.Nz)

			for i := 0; i < rows; i++ {
				p.Trz.Transform(coefsz, m.RowView(i))
				Evaluate(interpm.RowView(i), coefsz, eqpz)
			}

			col := make([]float64, p.Nr+1)

			for i := range eqpz {
				gridpoints := interpm.Col(make([]float64, rows), i)
				p.Trr.Transform(coefsr, gridpoints)
				Evaluate(col, coefsr, eqpr)
				resm.SetCol(i, col)
			}

			points := make([]*Point, (p.Nr+1)*(p.Nz+1))

			for i, r := range eqpr {
				for j, z := range eqpz {
					points[i*(p.Nz+1)+j] = &Point{
						X: (r + 1.0) / 2.0 * (p.flow.Rad(time) - Settings.Np.Offset),
						Y: (z + 1.0) * p.flow.Surf((r+1.0)/2.0, time) / 2.0,
						I: i,
						J: j,
					}
				}
			}

			Write(out, resm, points)

			out.Close()
		}
		done <- true
	}()
	return done
}

func Write(out io.Writer, vals *mat64.Dense, points []*Point) {
	_, c := vals.Dims()
	for i, p := range points {
		fmt.Fprintln(out, p.X, p.Y, vals.At(p.I, p.J))
		if (i+1)%c == 0 && i > 0 {
			fmt.Fprintln(out)
		}
	}
}

func WriteMat(out io.Writer, vals *mat64.Dense, xs, ys []float64) {
	for i, x := range xs {
		for j, y := range ys {
			fmt.Fprintln(out, x, y, vals.At(i, j))
		}
		fmt.Fprintln(out)
	}
}

func Evaluate(dst, spectral, points []float64) {
	for j := range dst {
		dst[j] = 0.0
		for k, c := range spectral {
			dst[j] += c * Chebyshev(k, points[j])
		}
	}
}
