package lava

import (
	"math"
)

const (
	g     = 9.8
	delta = 1e-3
)

type Params struct {
	Epsilon   float64
	Sigma     float64
	Lambda    float64
	LambdaAir float64
	Tcold     float64
	Tinit     float64
	Rho       float64
	Bd        float64
	Cp        float64
	Mu        float64
	Q         float64
	N         float64
	Beta      float64
	VelOffset float64

	l float64
	k float64

	a1 float64
	a2 float64
	b1 float64
	b4 float64
	c0 float64
	g0 float64
}

type Flow struct {
	*Params
}

func NewFlow(p *Params) (f *Flow) {
	f = &Flow{p}

	if f.Bd > 0.0 {
		f.l = 2.0 + 3.0/(f.N-3.0)
		f.k = 1.0 + 2.0/(f.N-3.0)
		f.g0 = 0.5
		f.a1 = math.Sqrt2 * math.Pow(f.l/f.Bt(), 1.0/(2.0*f.N))
		f.a2 = -math.Pow(f.a1, 4.0) / (6.0 * f.N * f.l)
		for gprev := 1.0; math.Abs(f.g0-gprev) > 1e-14; gprev = f.g0 {
			f.a1 = math.Sqrt2 * math.Pow(f.l/f.Bt(), 1.0/(2.0*f.N))
			f.a2 = -math.Pow(f.a1, 4.0) / (6.0 * f.N * f.l)
			f.g0 = 1.0 / 30.0 * (8*f.a1 + 5*f.a2)
		}
	} else {
		f.l = 1.0 / 8.0 * (3.0*f.Beta + 1)
		f.k = 1.0 / 4.0 * (p.Beta - 1.0)
		f.b1 = math.Cbrt(3.0 * f.l)
		f.b4 = f.b1 / 8.0 * (2.0/3.0 + f.k/f.l)
		f.g0 = 9.0 / 140.0 * (5.0*f.b1 + 2.0*f.b4)
	}

	f.c0 = math.Pow(3.0/(2.0*math.Pi*f.g0), 1.0/4.0)

	return
}

func (f *Flow) V(xi, z, t float64) float64 {
	a := f.Rho * g / f.Mu
	rt := f.Rad(t)
	xi = (1.0-f.VelOffset)*xi + f.VelOffset

	dhx := f.dhdx(xi, t) / rt
	res := 0.5 * a * dhx * z * (z - 2*f.Surf(xi, t))
	if f.Bd > 0.0 {
		res += f.Bd * math.Pow(-a*dhx*f.Surf(xi, t), f.N)
	}
	return res
}

func (f *Flow) W(xi, z, t float64) float64 {
	xin := (1.0-f.VelOffset)*xi + f.VelOffset

	res := 0.0
	rz := 0.0
	rt := f.Rad(t)
	var dr float64
	for i := 0; i <= 50; i++ {
		if xi+delta < 1.0 {
			dr = ((xin+delta)*(f.V(xi+delta, rz, t)) -
				xin*f.V(xi, rz, t)) / delta
		} else {
			dr = (xin*(f.V(xi, rz, t)) -
				(xin-delta)*f.V(xi-delta, rz, t)) / delta
		}
		res += -z / 50.0 * 1.0 / (xin * rt) * dr
		rz += z / 50.0
	}
	return res
}

func (f *Flow) Surf(xi, t float64) float64 {
	if f.Bd > 0.0 {
		return f.c0 * math.Pow(t, f.k) *
			(f.a1*math.Sqrt(1-xi) + f.a2*(1-xi))
	}
	return f.c0 * math.Pow(t, f.k) *
		math.Cbrt(1-xi) * (f.b1 + f.b4*(1-xi))

}

func (f *Flow) dhdx(xi, t float64) float64 {
	if f.Bd > 0.0 {
		return f.c0 * math.Pow(t, f.k) *
			(-f.a1/math.Sqrt(1.0-xi) - f.a2)
	}
	crt := math.Cbrt(1 - xi)
	return f.c0 * math.Pow(t, f.k) *
		(-(f.b1 + 3.0*f.b4*(1.0-xi) - f.b4*xi + f.b4) /
			(3.0 * crt * crt))

}

func (f *Flow) Bt() float64 {
	return math.Pow(3.0, (5.0*f.N+1.0)/8.0) *
		math.Pow(2.0*math.Pi*f.g0, -(f.N-3.0)/8.0) * f.B()
}

func (f *Flow) B() float64 {
	a := f.Rho * g / f.Mu
	return f.Bd * math.Pow(f.Q, (f.N-3.0)/8.0) *
		math.Pow(a, (3.0*f.N-1.0)/8.0)
}

func (f *Flow) Rad(t float64) float64 {
	return math.Pow(3.0*math.Pow(2.0*math.Pi*f.g0, 3.0), -1.0/8.0) *
		math.Pow(t, f.l)
}

func (f *Flow) HeatFlow(temperature float64) float64 {
	return -f.Epsilon*f.Sigma/f.Lambda*
		(math.Pow(temperature, 4.0)-math.Pow(f.Tcold, 4.0)) -
		f.LambdaAir/f.Lambda*
			math.Pow(temperature-f.Tcold, 4.0/3.0)
}

func (f *Flow) HeatFlowDeriv(temperature float64) float64 {
	return -4.0 / (3.0 * f.Lambda) *
		(f.LambdaAir*math.Cbrt(temperature-f.Tcold) +
			3.0*f.Sigma*f.Epsilon*math.Pow(temperature, 3.0))
}
