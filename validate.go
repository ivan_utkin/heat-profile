package main

import (
	"errors"
	"fmt"
	"github.com/krjackvinator/heat-profile/lava"
	"github.com/krjackvinator/heat-profile/numeric"
)

func Validate(i interface{}) error {
	switch s := i.(type) {
	case *lava.Params:
		return validateLava(s)
	case *numeric.Params:
		return validateNumeric(s)
	default:
		return errors.New("heat-profile: cannot resolve type")
	}
}

func validateLava(p *lava.Params) error {
	if !isPositive(p.Epsilon) {
		return positiveError("epsilon")
	}
	if !isPositive(p.Sigma) {
		return positiveError("sigma")
	}
	if !isPositive(p.Lambda) {
		return positiveError("lambda")
	}
	if !isPositive(p.LambdaAir) {
		return positiveError("lambdaAir")
	}
	if !isPositive(p.Tcold) {
		return positiveError("tcold")
	}
	if !isInInterval(p.Tinit, p.Tcold, p.Tcold+5000.0) {
		return intervalError("tInit", p.Tcold, p.Tcold+5000.0)
	}
	if !isPositive(p.Rho) {
		return positiveError("rho")
	}
	if isNegative(p.Bd) {
		return nonNegativeError("bd")
	}
	if !isPositive(p.Cp) {
		return positiveError("cp")
	}
	if !isPositive(p.Mu) {
		return positiveError("mu")
	}
	if !isPositive(p.Q) {
		return positiveError("q")
	}
	if isPositive(p.Bd) {
		if !isInInterval(p.N, 0, 1.4) && !isMore(p.N, 3.0) {
			return fmt.Errorf(
				"heat-profile: n must be in range from %v to %v or more than %v",
				0, 1.4, 3.0)
		}
	} else {
		if !isPositive(p.Beta) {
			return positiveError("beta")
		}
	}
	if !isInInterval(p.VelOffset, 0.0, 1.0) {
		return intervalError("velOffset", 0.0, 1.0)
	}
	return nil
}

func validateNumeric(p *numeric.Params) error {
	if !isPositive(float64(p.Nr)) {
		return positiveError("nr")
	}
	if !isPositive(float64(p.Nz)) {
		return positiveError("nz")
	}
	if !isMore(float64(p.Nt), 1.0) {
		return moreError("nt", 1.0)
	}
	if !isPositive(float64(p.ResR)) {
		return positiveError("resR")
	}
	if !isPositive(float64(p.ResZ)) {
		return positiveError("resZ")
	}
	if isNegative(p.Tstart) {
		return nonNegativeError("tStart")
	}
	if !isMore(p.Tend, p.Tstart) {
		return moreError("tEnd", p.Tstart)
	}
	if !isPositive(p.Offset) {
		return positiveError("offset")
	}
	return nil
}

func isPositive(v float64) bool {
	if v > 0.0 {
		return true
	}
	return false
}

func isNegative(v float64) bool {
	return !isPositive(v) && !isZero(v)
}

func isZero(v float64) bool {
	if v == 0.0 {
		return true
	}
	return false
}

func isInInterval(v, s, e float64) bool {
	if v > s && v < e {
		return true
	}
	return false
}

func isMore(v, s float64) bool {
	if v > s {
		return true
	}
	return false
}

func isLess(v, e float64) bool {
	if v < e {
		return true
	}
	return false
}

func positiveError(what string) error {
	return fmt.Errorf("heat-profile: %v should be positive", what)
}

func nonNegativeError(what string) error {
	return fmt.Errorf("heat-profile: %v should be non-negative", what)
}

func intervalError(what string, le, ge float64) error {
	return fmt.Errorf("heat-profile: %v should be in range from %v to %v",
		what, le, ge)
}

func moreError(what string, v float64) error {
	return fmt.Errorf("heat-profile: %v should be more than %v", what, v)
}
