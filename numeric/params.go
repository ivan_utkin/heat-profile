package numeric

type Params struct {
	Nr   int
	Nz   int
	Nt   int
	ResR int
	ResZ int

	Tstart float64
	Tend   float64

	Offset float64
}
