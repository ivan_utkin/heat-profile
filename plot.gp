reset

# Uncomment if you want to plot in file
 set terminal pngcairo size 1024,768 enhanced font 'Verdana, 8' 
 set output 'out.png'

set xlabel 'r'
set ylabel 'z'

set xtics scale 0.5
set ytics scale 0.5

set view map

set palette rgb 33,13,10
set cbrange [100:1100]

set multiplot layout 3,4

unset key

# Uncomment to fix xrange
# set xrange [0:160]
set yrange [0:1.5]

 set size square

set title 't=100c'
splot 'output/100.000.txt' w pm3d 

set title 't=2500c'
splot 'output/2500.000.txt' w pm3d

set xtics 10
set title 't=5000c'
splot 'output/5000.000.txt' w pm3d

set title 't=10000c'
splot 'output/10000.000.txt' w pm3d 

set title 't=25000c'
splot 'output/25000.000.txt' w pm3d

set xtics 60
set title 't=50000c'
splot 'output/50000.000.txt' w pm3d

set title 't=75000c'
splot 'output/75000.000.txt' w pm3d

set title 't=100000c'
splot 'output/100000.000.txt' w pm3d

set title 't=125000c'
splot 'output/125000.000.txt' w pm3d

set title 't=150000c'
splot 'output/150000.000.txt' w pm3d

set title 't=175000c'
splot 'output/175000.000.txt' w pm3d

set title 't=200000c'
splot 'output/200000.000.txt' w pm3d

unset multiplot
 unset output
 set terminal windows

reset