package operator

import (
	"github.com/gonum/matrix/mat64"
	"github.com/krjackvinator/heat-profile/lava"
)

type Operator interface {
	Apply(vec, dst []float64)
	Update(time float64)
}

type matrixOp struct {
	matrix *mat64.Dense
}

type lavaOp struct {
	lava.Params
}

type Advection struct {
	matrixOp
	lavaOp
	timestep float64
	time     float64
	grid     Grid
}

type Grid interface {
}

func (a *Advection) Update(time float64) {
	a.time = time
}

func (a *Advection) Apply(vec, dst []float64, time float64) {
	n := len(vec)
	mat64.NewDense(n, 1, dst).Mul(a.matrix, mat64.Vec(vec))
	for i := range dst {
		xi, eta := a.grid.At(i)
		dst[i] *= lava.V()
	}
}
