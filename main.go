package main

import (
	"encoding/json"
	"flag"
	"github.com/cheggaaa/pb"
	"github.com/gonum/blas/goblas"
	"github.com/gonum/floats"
	"github.com/gonum/matrix/mat64"
	"github.com/krjackvinator/heat-profile/lava"
	"github.com/krjackvinator/heat-profile/numeric"
	"io/ioutil"
	"log"
	"math"
	"runtime"
)

var Settings struct {
	Lp *lava.Params    `json:"problemParams"`
	Np *numeric.Params `json:"numericParams"`
}

var filename string

func init() {
	flag.StringVar(&filename, "filename", "config.json",
		"name of file with parameters of the problem")
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	flag.Parse()

	mat64.Register(goblas.Blas{})

	log.Println("Reading config...")
	config, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}
	if err := json.Unmarshal(config, &Settings); err != nil {
		log.Fatal(err)
	}

	log.Println("Validating parameters...")
	if err := Validate(Settings.Lp); err != nil {
		log.Fatal(err)
	}
	if err := Validate(Settings.Np); err != nil {
		log.Fatal(err)
	}

	tstep := (Settings.Np.Tend - Settings.Np.Tstart) / float64(Settings.Np.Nt)

	Nr := Settings.Np.Nr
	Nz := Settings.Np.Nz

	f := lava.NewFlow(Settings.Lp)

	pointsr := GaussLobatto(Nr)
	pointsz := GaussLobatto(Nz)
	prev := NewDense(Nr+1, Nz+1)

	ImposeIC(pointsr, pointsz, prev)

	derivmatz2 := DerivMat(2, pointsz)
	derivmatz1 := DerivMat(1, pointsz)

	derivmatr1 := DerivMat(1, pointsr)

	outch := make(chan *mat64.Dense, 5)
	timech := make(chan float64, 5)

	toSpectralr := NewTransformer(transform(pointsr))
	toSpectralz := NewTransformer(transform(pointsz))

	writer := Writer{f, toSpectralr, toSpectralz,
		outch, timech,
		Settings.Np.ResR, Settings.Np.ResZ}

	done := writer.Run()

	initm := mat64.DenseCopyOf(prev)

	outch <- initm
	timech <- Settings.Np.Tstart

	wm := NewSquareDense(Nz + 1)
	temp := NewSquareDense(Nz + 1)
	operatorz := NewSquareDense(Nz + 1)

	prevopz := mat64.DenseCopyOf(operatorz)

	rhsz := make([]float64, Nz+1)
	rhsr := make([]float64, Nr+1)

	currz := make([]float64, Nz+1)
	lcr := make([]float64, Nr+1)

	uprev := make([]float64, Nr+1)
	uold := make([]float64, Nz+1)

	unorm := mat64.DenseCopyOf(prev)
	uback := mat64.DenseCopyOf(prev)

	log.Println("Starting to solve...")
	bar := pb.StartNew(Settings.Np.Nt)
	for t := Settings.Np.Tstart + tstep; t <= Settings.Np.Tend; t += tstep {
		bar.Increment()
		norm := 1.0
		uback.Clone(prev)
		rt := f.Rad(t)
		for norm > 1e-2 {
			norm = 0.0
			for i := 0; i <= Nr; i++ {
				u := prev.RowView(i)
				ub := uback.RowView(i)
				xi := (pointsr[i] + 1.0) / 2.0 * (rt - Settings.Np.Offset) / rt
				for j := 0; j <= Nz; j++ {
					z := (pointsz[j] + 1.0) / 2.0 * f.Surf(xi, t)
					wm.Set(j, j, -f.W(xi, z, t-tstep/2.0)/
						(f.Rho*f.Cp))
				}
				temp.Mul(wm, derivmatz1)
				operatorz.Scale(f.Lambda/(f.Rho*f.Cp), derivmatz2)
				operatorz.Add(operatorz, temp)
				prevopz.Clone(operatorz)
				for k := 0; k <= Nz; k++ {
					operatorz.Set(k, k, operatorz.At(k, k)-2.0/tstep)
				}
				mat64.NewDense(Nz+1, 1, uold).Mul(operatorz, mat64.Vec(u))
				lc := operatorz.RowView(0)
				derivmatz1.Row(lc, 0)
				rc := operatorz.RowView(Nz)
				derivmatz1.Row(rc, Nz)
				operatorz.Set(0, 0, operatorz.At(0, 0)-f.HeatFlowDeriv(u[0]))
				mat64.NewDense(Nz+1, 1, rhsz).Mul(prevopz, mat64.Vec(ub))
				floats.Scale(-1.0, rhsz)
				floats.AddScaled(rhsz, -2.0/tstep, u)
				floats.AddScaled(rhsz, -1.0, uold)
				der1 := derivmatz1.RowView(0)
				dern := derivmatz1.RowView(Nz)
				rhsz[0] = f.HeatFlow(u[0]) - floats.Dot(der1, u)
				rhsz[Nz] = -floats.Dot(dern, u)
				currm, err := mat64.Solve(operatorz, mat64.Vec(rhsz))
				if err != nil {
					log.Fatal(err)
				}
				currm.Col(currz, 0)
				floats.Add(u, currz)
			}

			for i := 0; i <= Nz; i++ {
				u := prev.Col(make([]float64, Nr+1), i)
				copy(uprev, u)
				un := unorm.Col(make([]float64, Nr+1), i)
				mat64.NewDense(Nr+1, 1, rhsr).Mul(derivmatr1, mat64.Vec(u))
				for j := range rhsr {
					xi := (pointsr[j] + 1.0) / 2.0 * (rt - Settings.Np.Offset) / rt
					z := (pointsz[i] + 1.0) / 2.0 * f.Surf(xi, t)
					rhsr[j] *= -f.V(xi, z, t) /
						(f.Rho * f.Cp)
				}
				floats.AddScaled(u, tstep, rhsr)

				derivmatr1.Row(lcr, Nr)
				lcr[Nr] = 0.0
				d := floats.Dot(lcr, u)
				u[Nr] = -d / derivmatr1.At(Nr, Nr)

				snorm := floats.Distance(un, u, 2)
				norm += snorm * snorm
				prev.SetCol(i, u)
			}
			norm = math.Sqrt(norm) / float64(Nr*Nz)
			unorm.Clone(prev)
		}
		s := mat64.DenseCopyOf(prev)

		outch <- s
		timech <- t
	}

	close(outch)
	close(timech)

	<-done
	bar.FinishPrint("Done!")
}

func EqSpPoints(n int) []float64 {
	res := make([]float64, n+1)
	for i := range res {
		res[i] = -2.0*float64(i)/float64(n) + 1.0
	}
	return res
}

func ImposeIC(pointsx, pointsy []float64, res *mat64.Dense) {
	for i, x := range pointsx {
		for j, y := range pointsy {
			res.Set(i, j, initial(x, y))
		}
	}
}

func Lc(u []float64, t float64) float64 {
	return 0.0
}

func Rc(u []float64, t float64) float64 {
	return 0.0
}

func initial(x, y float64) float64 {
	return Settings.Lp.Tinit
}

func GaussLobatto(n int) (c []float64) {
	c = make([]float64, n+1)
	for i := range c {
		c[i] = math.Cos(math.Pi * float64(i) / float64(n))
	}
	return
}

func Basis(ps []float64) []float64 {
	n := len(ps) - 1
	b := make([]float64, n+1)
	for i, x := range ps {
		b[i] = math.Cos(float64(n) * math.Acos(x))
	}
	return b
}

func Chebyshev(n int, x float64) float64 {
	return math.Cos(float64(n) * math.Acos(x))
}

func Basisdx(ps []float64) []float64 {
	n := len(ps) - 1
	fn := float64(n)
	b := make([]float64, n+1)
	for i, x := range ps {
		acos := math.Acos(x)
		b[i] = fn * math.Sin(fn*acos) /
			math.Sin(acos)
	}
	return b
}

func Basisdxx(ps []float64) []float64 {
	n := len(ps) - 1
	fn := float64(n)
	b := make([]float64, n+1)
	for i, x := range ps {
		acos := math.Acos(x)
		sin := math.Sin(acos)
		b[i] = fn*fn*math.Cos(fn*acos)/(sin*sin) +
			fn*math.Cos(acos)*math.Sin(fn*acos)/(sin*sin*sin)
	}
	return b
}

func DerivMat(o int, points []float64) (m *mat64.Dense) {
	n := len(points) - 1
	m = NewSquareDense(n + 1)

	corner := (1.0 + 2.0*float64(n)*float64(n)) / 6.0
	m.Set(0, 0, corner)
	m.Set(n, n, -corner)

	for j := 0; j <= n; j++ {
		xj := points[j]
		for i := 0; i <= n; i++ {
			if i == j {
				if (j > 0) && (j < n) {
					diag := -xj / (2 * (1 - xj*xj))
					m.Set(i, j, diag)
				}
			} else {
				xi := points[i]
				v := minOne(i+j) * getp(i, n) /
					(getp(j, n) * (xi - xj))
				m.Set(i, j, v)
			}
		}
	}

	pow(o, m)
	return
}
