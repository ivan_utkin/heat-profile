package main

import (
	"github.com/gonum/matrix/mat64"
)

func NewDense(r, c int) *mat64.Dense {
	return mat64.NewDense(r, c, make([]float64, r*c))
}

func NewSquareDense(n int) *mat64.Dense {
	return NewDense(n, n)
}

func minOne(k int) float64 {
	if k%2 == 0 {
		return 1.0
	}
	return -1.0
}

func getp(i, n int) float64 {
	if i == 0 || i == n {
		return 2.0
	}
	return 1.0
}

func pow(o int, m *mat64.Dense) {
	t := mat64.DenseCopyOf(m)
	for ; o > 1; o-- {
		m.Mul(m, t)
	}
}
