package main

import (
	"github.com/gonum/matrix/mat64"
	"math"
)

type Transformer struct {
	operator *mat64.Dense
}

func NewTransformer(op *mat64.Dense) *Transformer {
	return &Transformer{op}
}

func (t *Transformer) Transform(dst, src []float64) {
	mat64.NewDense(len(dst), 1, dst).Mul(t.operator, mat64.Vec(src))
}

func weight(i, n int) float64 {
	if i == 0 || i == n {
		return math.Pi / (2.0 * float64(n))
	}
	return math.Pi / float64(n)
}

func autoscalar(i int) float64 {
	if i == 0 {
		return math.Pi
	}
	return math.Pi / 2.0
}

func transform(points []float64) (m *mat64.Dense) {
	n := len(points) - 1
	m = NewSquareDense(n + 1)

	for i := 0; i <= n; i++ {
		phii := autoscalar(i)
		for j := 0; j <= n; j++ {
			m.Set(i, j, weight(j, n)*Chebyshev(i, points[j])/phii)
		}
	}
	return
}
